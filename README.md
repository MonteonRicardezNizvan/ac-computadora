# Arquitectura de Computadoras
A continuación se presentarán los dos mapas conceptuales desarrollados para la actividad, ambos basados en la información presentada en los videos de la plataforma Moodle.

---

## Von Neumann vs Harvard
Este mapa conceptual abarca dos arquitecturas de computadoras clásicas, la de Von Neumann y Harvard. A continuación se presentarán las características principales de cada una de ellas, así como un poco de historia de las mismas.

```plantuml
@startmindmap
*[#lightcoral] ARQUITECTURA DE\nCOMPUTADORAS
** Estructura\noperacional\nde un sistema
***_ existen modelos\nque dieron origen\na la computación
****[#lightblue] MODELOS DE\nARQUITECURA\nCLÁSICA
*****[#lightblue] HARVARD
******_ configuración de\ncomputadora donde
******* Los datos de\ninstrucciones están\nen celdas separadas\nde memorias
********[#lightblue] ESTRUCTURA
********* Procesador
********* Memoria de\ninstrucciones
********* Memoria de\ndatos
********* Buses para\ncada memoria
********[#lightblue] CARACTERÍSTICAS
********* Dos memorias con\nbuses independientes\nconectados al\nprocesador
********* Se puede contener\ndiferente información\nen la misma ubicación\nde las diferentes\nmemorias
******_ desarrollado por
******* Howard Aiken
********_ creó una computadora\nbasada en relés
********* Harvard Mark I
*****[#lightblue] VON NEUMANN
******_ desarrollado por
******* John Von Neumann
******** Pionero de las ciencias\nde la comunicación
********* Contribuyó ideas para\nla computadora\nENIAC
******_ es la
******* Arquitectura\nde los\nordenadores\nactuales
********[#lightblue] CARACTERÍSTICAS
********* Bit como unidad\n de medida
********* Consiste en\ndiferentes bloques\nfuncionales
********* Bloques conectados\nentre sí
********* Una sola memoria\nprincipal
********[#lightblue] ESTRUCTURA
********* Procesador, parte central
********* Memoria principal, RAM\n en la actualidad
********* Buses que permiten la\ncomunicación entre\nlos bloques del sistema
********** De datos
********** De dirección
********** De control
********* Periféricos
********** Entrada
********** Salida

@endmindmap
```

## Supercomputadoras
Este mapa conceptual describe el paso de las supercomputadoras en México, la historia y características principales de cada una de ellas. De igual manera se hace una pequeña mención a los usos principales de estas mismas.

```plantuml
@startmindmap
*[#lightcoral] SUPERCOMPUTADORAS
** Ordenadores de\nalto desempeño,\nextremadamente\npotentes
***[#lightblue] HISTORIA
**** Las primeras computadoras\nse fabricaban con bulbos
***** Con la invensión del transistor\nlas computadoras redujeron\nsu tamaño
****** Los circuitos integrados\npermitieron acelerar la\nvelocidad de computo
***_ en
****[#lightblue] MÉXICO
***** El rector Nabor Carrillo\ntenía interés en las\nsupercomputadoras
******_ para ello,\nla UNAM tuvo en
*******_ 1958
********[#lightblue] IBM 650
********* Procesaba 1000\noperaciones por\nsegundo
*******_ 1991
********[#lightblue] CRAY 432
********* Primer supercomputadora\nde América Latina
********** Equivalente a 2000\noredenadores de\noficina
*******_ 2007
********[#lightblue] KanBalam
********* Realiza 7 billones\nde operaciones\npor segundo
*******_ 2012
********[#lightblue] Miztli
********* 228 billones de\noperaciones por\nsegundo
********[#lightblue] Proyecto\nXiuhcoatl
********* Laboratorio Nacional\nde Cómputo de Alto\nDesempeño
********** 2da computadora\nmás rápida de\nLatinoamérica
*****[#lightblue] BUAP
****** Cuenta con una de las 5\ncomputadoras más\npoderosas de Latinoamérica
******* 2000 millones de operaciones por segundo
***[#lightblue] USOS
****_ principalmente
***** Investigación científica
****** Astronomía
****** Química cuántica
****** Nanotecnología
****** Medicina
@endmindmap
```

---
Trabajo para la materia de Arquitectura de Computadoras.
